@echo off
cls

set MAIN_PATH="%~dp0.."
set NUM_PLAYERS=%1
set FILE_NAME=%2
set PATH=%MAIN_PATH%\Java\bin;%MAIN_PATH%\Java\ant-1.9.6\bin;c:\Windows

ant run -Ddrive-letter=%MAIN_PATH% -Dnum-players=%NUM_PLAYERS% -Dfile-name=%FILE_NAME%